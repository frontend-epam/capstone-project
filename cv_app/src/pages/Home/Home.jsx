import React from "react";
import styles from "./Home.module.css";
import PhotoBox from "../../components/PhotoBox/PhotoBox";
import Button from "../../components/Button/Button";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className={styles.home}>
      <div className={styles.content}>
        <PhotoBox
          name="John Doe"
          title="Programmer. Creative. Innovator"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
          avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
        />
        <Link to="/inner">
          <Button text={"Know more"} />
        </Link>
      </div>
    </div>
  );
};

export default Home;
