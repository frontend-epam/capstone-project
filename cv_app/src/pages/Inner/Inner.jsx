import ReactDOM from "react-dom";
import { useState } from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faFacebook,
  faSkype,
  faTwitter,
  fab,
} from "@fortawesome/free-brands-svg-icons";
import {
  faChevronLeft,
  faEnvelope,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./Inner.module.css";
import Experience from "../../components/Expertise/Experience";
import { Box } from "../../components/Box/Box";
import TimeLine from "../../components/TimeLine/TimeLine";
import Feedback from "../../components/Feedback/Feedback";
import Button from "../../components/Button/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ContactInfo from "../../components/Contacts/Contacts";
import Contacts from "../../components/Contacts/Contacts";
import Navigation from "../../components/Navigation/Navigation";
import Portfolio from "../../components/Portfolio/Portfolio";
import PhotoBox from "../../components/PhotoBox/PhotoBox";
import Panel from "../../components/Panel/Panel";
import useIntersectionObserver from "../../utils/useIntersectionObserver";

library.add(fab, faChevronLeft);

const experienceData = [
  {
    date: "2013-2014",
    info: {
      company: "Google",
      job: "Front-end developer / php programmer",
      description:
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
    },
  },
  {
    date: "2012",
    info: {
      company: "Twitter",
      job: "Web developer",
      description:
        "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim",
    },
  },
];

const feedbackData = [
  {
    feedback:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ",
    reporter: {
      photoUrl:
        "https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg",
      name: "John Doe",
      citeUrl: "https://www.citeexample.com",
    },
  },
  {
    feedback:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
    reporter: {
      photoUrl:
        "https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg",
      name: "John Doe",
      citeUrl: "https://www.citeexample.com",
    },
  },
];

const timelineData = [
  {
    date: 2001,
    title: "Title 0",
    text: "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n",
  },
  {
    date: 2000,
    title: "Title 1",
    text: "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n",
  },
  {
    date: 2012,
    title: "Title 2",
    text: "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n",
  },
];

const contactDetails = [
  {
    icon: faPhone,
    value: "500 342 242",
    link: "tel:+500342242",
    important: true,
  },
  {
    icon: faEnvelope,
    value: "office@kamsolutions.pl",
    link: "mailto:office@kamsolutions.pl",
    important: true,
  },
  {
    icon: faTwitter,
    label: "Twitter",
    value: "https://twitter.com/wordpress",
    link: "https://twitter.com/wordpress",
  },
  {
    icon: faFacebook,
    label: "Facebook",
    value: "https://www.facebook.com/facebook",
    link: "https://www.facebook.com/facebook",
  },
  {
    icon: faSkype,
    label: "Skype",
    value: "kamsolutions.pl",
    link: "skype:kamsolutions.pl",
  },
];
const Inner = () => {
  const [isPanelHidden, setIsPanelHidden] = useState(false);
  const [activeSection, setActiveSection] = useState(
    window.location.hash || "#about"
  );

  useIntersectionObserver(setActiveSection);

  const hidePanel = (newStatus) => {
    setIsPanelHidden(newStatus);
  };

  return (
    <div className={styles.container}>
      <Panel hidePanel={hidePanel} activeSection={activeSection} />
      <div
        className={`${isPanelHidden ? styles.content_hiddenPanel : styles.content}`}
      >
        <section id="about" data-section>
          <Box
            title="About me"
            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque"
          />
        </section>
        <section id="education" data-section>
          <Box title="Education" content={<TimeLine data={timelineData} />} />
        </section>
        <section id="experience" data-section>
          <Box
            title="Experience"
            content={<Experience experienceList={experienceData} />}
          />
        </section>
        <section id="portfolio" data-section>
          <Box title="Portfolio" content={<Portfolio />} />
        </section>
        <section id="contacts" data-section>
          <Box
            title="Contacts"
            content={<Contacts contactDetails={contactDetails} />}
          />
        </section>
        <section id="feedbacks" data-section>
          <Box title="Feedbacks" content={<Feedback data={feedbackData} />} />
        </section>
      </div>
    </div>
  );
};

export default Inner;
