import { useEffect } from "react";

const useIntersectionObserver = (setActiveSection) => {
  useEffect(() => {
    const options = {
      root: null,
      rootMargin: "0px 0px -90% 0px", // Adjust root margin to trigger when the top of the element crosses the top of the viewport
      threshold: 0,
    };

    const observerCallback = (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          setActiveSection(`#${entry.target.id}`);
        }
      });
    };

    const observer = new IntersectionObserver(observerCallback, options);
    const sections = document.querySelectorAll("section[data-section]");

    sections.forEach((section) => {
      observer.observe(section);
    });

    return () => {
      sections.forEach((section) => {
        observer.unobserve(section);
      });
    };
  }, [setActiveSection]);
};

export default useIntersectionObserver;