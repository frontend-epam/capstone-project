import React from "react";
import styles from "./TimeLine.module.css";
import Info from "../Info/Info";

const TimeLine = ({ data }) => {
  return (
    <div className={styles.timeline}>
      <ul className={styles.timelineList}>
        {data.map((item, index) => (
          <li key={index}>
            <div className={styles.timelineDate}>{item.date}</div>
            <div className={styles.timelineEvent}>
              <Info title={item.title} text={item.text} />
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TimeLine;
