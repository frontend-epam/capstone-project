// Expertise.js

import React from "react";
import PropTypes from "prop-types";

import styles from "./Experience.module.css";

const Experience = ({ experienceList }) => {
  return (
    <ul className={styles.experienceList}>
      {experienceList.map((item, index) => (
        <li key={index} className={styles.experienceItem}>
          <div className={styles.experienceListDate}>
            <h3>{item.info.company}</h3>
            <span className={styles.date}>{item.date}</span>
          </div>
          <div className={styles.experienceListInfo}>
            <h3>{item.info.job}</h3>
            <p>{item.info.description}</p>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default Experience;
