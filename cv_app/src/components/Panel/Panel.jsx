import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./Panel.module.css";
import PhotoBox from "../PhotoBox/PhotoBox";
import Navigation from "../Navigation/Navigation";
import Button from "../Button/Button";
import { faChevronLeft, faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

const Panel = ({ hidePanel, activeSection }) => {
  const [showPanel, setShowPanel] = useState(true);

  const togglePanel = () => {
    hidePanel(showPanel);
    setShowPanel(!showPanel);
  };

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
      if (window.innerWidth < 600) {
        hidePanel(true);
        setShowPanel(false);
      } else {
        hidePanel(false);
        setShowPanel(true);
      }
    };
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [hidePanel]);

  return (
    <div>
      <nav className={`${!showPanel ? styles.panel_hidden : styles.panel}`}>
        <div className={styles.photoBox}>
          <PhotoBox
            name="John Doe"
            avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
            fontStyle="smaller-font"
          />
        </div>
        <div className={styles.navigation}>
          <Navigation activeSection={activeSection} />
        </div>
        <Link to="/">
          <Button icon={faChevronLeft} text="Go back" />
        </Link>
      </nav>
      <button
        className={`${!showPanel ? styles.toggleButton_hidden : styles.toggleButton}`}
        onClick={togglePanel}
      >
        <FontAwesomeIcon icon={faBars} className={styles.icon} />
      </button>
    </div>
  );
};

Panel.propTypes = {
  hidePanel: PropTypes.func.isRequired,
  activeSection: PropTypes.string.isRequired,
};

export default Panel;
