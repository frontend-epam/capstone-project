import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPen,
  faGraduationCap,
  faBriefcase,
  faUser,
  faComment,
  faLocationArrow,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./Navigation.module.css";

const Navigation = ({ activeSection }) => {
  return (
    <div data-nav="list">
      <ul className={styles.navigation}>
        <li className={activeSection === "#about" ? styles.active : ""}>
          <FontAwesomeIcon icon={faUser} className={styles.icon} />
          <a href="#about">
            <span>About me</span>
          </a>
        </li>
        <li className={activeSection === "#education" ? styles.active : ""}>
          <FontAwesomeIcon icon={faGraduationCap} className={styles.icon} />
          <a href="#education">
            <span>Education</span>
          </a>
        </li>
        <li className={activeSection === "#experience" ? styles.active : ""}>
          <FontAwesomeIcon icon={faBriefcase} className={styles.icon} />
          <a href="#experience">
            <span>Experience</span>
          </a>
        </li>
        <li className={activeSection === "#portfolio" ? styles.active : ""}>
          <FontAwesomeIcon icon={faPen} className={styles.icon} />
          <a href="#portfolio">
            <span>Portfolio</span>
          </a>
        </li>
        <li className={activeSection === "#contacts" ? styles.active : ""}>
          <FontAwesomeIcon icon={faLocationArrow} className={styles.icon} />
          <a href="#contacts">
            <span>Contacts</span>
          </a>
        </li>
        <li className={activeSection === "#feedbacks" ? styles.active : ""}>
          <FontAwesomeIcon icon={faComment} className={styles.icon} />
          <a href="#feedbacks">
            <span>Feedbacks</span>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Navigation;
