import React from 'react';

import styles from './Info.module.css';

const Info = ({ title, text }) => {
  return (
    <div className={styles.info}>
        {title && <h3>{title}</h3>}
        <p>{text}</p>
    </div>
  );
};

export default Info;
