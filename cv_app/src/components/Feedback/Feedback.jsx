import React from "react";
import styles from "./Feedback.module.css";
import Info from "../Info/Info";

const Feedback = ({ data }) => {
  return (
    <ul className={styles.feedback}>
      {data.map((item, index) => (
        <li key={index}>
          <blockquote>
            <div className={styles.feedbackInfo}>
              <Info text={item.feedback} />
            </div>
            <div className={styles.feedbackReporter}>
              <img
                className={styles.feedbackReporterPhoto}
                src={item.reporter.photoUrl}
                alt="Feedback reporter"
              />
              <p>
                {item.reporter.name},{" "}
                <a
                  href={item.reporter.citeUrl}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {new URL(item.reporter.citeUrl).hostname.replace("www.", "")}
                </a>
              </p>{" "}
            </div>
          </blockquote>
        </li>
      ))}
    </ul>
  );
};

export default Feedback;
