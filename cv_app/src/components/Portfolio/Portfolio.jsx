import React, { useEffect, useState } from "react";
import Isotope from "isotope-layout";

import card_1 from "../../assets/images/card_1.jpg";
import card_2 from "../../assets/images/card_2.jpg";
import card_3 from "../../assets/images/card_3.jpg";

import "./Portfolio.css";

const Portfolio = () => {
  const [selectedFilter, setSelectedFilter] = useState("*"); // State to store the selected filter item

  useEffect(() => {
    // Initialize Isotope on the portfolio container
    const portfolioGrid = document.querySelector(".portfolio-grid");
    const iso = new Isotope(portfolioGrid, {
      itemSelector: ".portfolio-item",
      layoutMode: "fitRows",
    });

    // Filter items when filter tabs are clicked
    const filterTabs = document.querySelectorAll(".filter-tab");
    filterTabs.forEach((tab) => {
      tab.addEventListener("click", () => {
        const filterValue = tab.getAttribute("data-filter");
        iso.arrange({ filter: filterValue });
        setSelectedFilter(filterValue); // Update the selected filter item in state
      });
    });
  }, []);

  return (
    <div className="portfolio">
      <div className="portfolio-filter">
        {/* Add filter tabs here */}
        <a
          className={`filter-tab ${selectedFilter === "*" && "selected"}`}
          data-filter="*"
        >
          All
        </a>
        <a>/</a>
        <a
          className={`filter-tab ${selectedFilter === ".filter-item1" && "selected"}`}
          data-filter=".filter-item1"
        >
          Code
        </a>
        <a>/</a>
        <a
          className={`filter-tab ${selectedFilter === ".filter-item2" && "selected"}`}
          data-filter=".filter-item2"
        >
          UI
        </a>
      </div>
      <div className="portfolio-grid">
        {/* Hardcoded portfolio items */}
        <div className="portfolio-item filter-item1">
          <div className="portfolio-item-content">
            <img src={card_1} alt="Card 1" />
            <div className="portfolio-item-info">
              <h3>Item 1</h3>
              <p>
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
                arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
                justo. Nullam dictum felis eu pede mollis{" "}
              </p>
              <a href="https://somesite.com">View source</a>
            </div>
          </div>
        </div>
        <div className="portfolio-item filter-item2">
          <div className="portfolio-item-content">
            <img src={card_2} alt="Card 2" />
            <div className="portfolio-item-info">
              <h3>Item 2</h3>
              <p>
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
                arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
                justo. Nullam dictum felis eu pede mollis{" "}
              </p>
              <a href="https://somesite.com">View source</a>
            </div>
          </div>
        </div>
        <div className="portfolio-item filter-item1">
          <div className="portfolio-item-content">
            <img src={card_3} alt="Card 3" />
            <div className="portfolio-item-info">
              <h3>Item 3</h3>
              <p>
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
                arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
                justo. Nullam dictum felis eu pede mollis{" "}
              </p>
              <a href="https://somesite.com">View source</a>
            </div>
          </div>
        </div>
        <div className="portfolio-item filter-item2">
          <div className="portfolio-item-content">
            <img src={card_1} alt="Card 4" />
            <div className="portfolio-item-info">
              <h3>Item 4</h3>
              <p>
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
                arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
                justo. Nullam dictum felis eu pede mollis{" "}
              </p>
              <a href="https://somesite.com">View source</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Portfolio;
