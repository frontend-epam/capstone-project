import React from 'react';

import styles from './Box.module.css';


export const Box = ({title, content}) => {
    return (
    <div className={styles.box}>
    <h2 className={styles.boxTitle}>{title}</h2>
    <div className={styles.boxContent}>{content}</div>
  </div>
  );
}
