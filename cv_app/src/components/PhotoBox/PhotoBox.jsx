import React from "react";
import styles from "./PhotoBox.module.css"; // Import CSS module for styling

const PhotoBox = ({ name, title, description, avatar, fontStyle }) => {
  const nameClass = `${styles.name} ${fontStyle}`;
  const nameStyle =
    fontStyle === "smaller-font"
      ? {
          fontSize: "16px",
          color: "#fff",
          fontWeight: "700",
          lineHeight: "120%",
        }
      : {};

  return (
    <div className={styles.photoBox}>
      <img src={avatar} alt={name} className={styles.avatar} />
      <div className={styles.info}>
        <h2 style={nameStyle} className={nameClass}>
          {name}
        </h2>
        {title && <p className={styles.title}>{title}</p>}
        {description && <p className={styles.description}>{description}</p>}
      </div>
    </div>
  );
};

export default PhotoBox;
