import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faTwitter,
  faSkype,
} from "@fortawesome/free-brands-svg-icons";
import styles from "./Contacts.module.css"; // Assuming you have a CSS module for styling

const Contacts = ({ contactDetails }) => {
  return (
    <address className={styles.address}>
      {contactDetails.map((detail, index) => (
        <dl key={index}>
          <dt>
            <FontAwesomeIcon icon={detail.icon} className={styles.icon} />
          </dt>
          <dd className={detail.important ? styles.important : ""}>
            {detail.label ? (
              <>
                <strong>{detail.label}</strong>
                <br />
                <a href={detail.link}>{detail.value}</a>
              </>
            ) : (
              <a href={detail.link} className={styles.boldLink}>
                <strong>{detail.value}</strong>
              </a>
            )}
          </dd>
        </dl>
      ))}
    </address>
  );
};

export default Contacts;
