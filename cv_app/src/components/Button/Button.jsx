import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./Button.module.css";

const Button = ({ icon, text, onClick }) => {
  return (
    <button className={styles.button} onClick={onClick}>
      {icon && <FontAwesomeIcon icon={icon} className={styles.icon} />}
      {text}
    </button>
  );
};

export default Button;
