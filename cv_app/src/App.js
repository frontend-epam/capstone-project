import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebook, faSkype, faTwitter, fab } from '@fortawesome/free-brands-svg-icons'
import { faChevronLeft, faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons'


import Experience from './components/Expertise/Experience';
import { Box } from './components/Box/Box';
import TimeLine from './components/TimeLine/TimeLine';
import Feedback from './components/Feedback/Feedback';
import Button from './components/Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ContactInfo from './components/Contacts/Contacts';
import Contacts from './components/Contacts/Contacts';
import Navigation from './components/Navigation/Navigation';
import Home from './pages/Home/Home';
import Inner from './pages/Inner/Inner';

import {Route, Routes} from 'react-router-dom';
library.add(fab, faChevronLeft)

const experienceData = [
  {
    date: '2013-2014',
    info: {
      company: 'Google',
      job: 'Front-end developer / php programmer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
    },
  },
  {
    date: '2012',
    info: {
      company: 'Twitter',
      job: 'Web developer',
      description: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim',
    },
  },
];

const feedbackData = [
  {
    feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
    reporter: {
      photoUrl: './user.jpg',
      name: 'John Doe',
      citeUrl: 'https://www.citeexample.com'
    }
  },
  {
    feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
    reporter: {
      photoUrl: './user.jpg',
      name: 'John Doe',
      citeUrl: 'https://www.citeexample.com'
    }
  }
];

const timelineData = [
  {
    date: 2001,
    title: 'Title 0',
    text: 'Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n',
  },
  {
    date: 2000,
    title: 'Title 1',
    text: 'Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n',
  },
  {
    date: 2012,
    title: 'Title 2',
    text: 'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n',
  },
];

const contactDetails = [
  { icon: faPhone, label: "Phone", value: "500 342 242", link: "tel:+500342242", important: true },
  { icon: faEnvelope, label: "Email", value: "office@kamsolutions.pl", link: "mailto:office@kamsolutions.pl", important: true },
  { icon: faTwitter, label: "Twitter", value: "https://twitter.com/wordpress", link: "https://twitter.com/wordpress" },
  { icon: faFacebook, label: "Facebook", value: "https://www.facebook.com/facebook", link: "https://www.facebook.com/facebook" },
  { icon: faSkype, label: "Skype", value: "kamsolutions.pl", link: "skype:kamsolutions.pl" }
];

function App() {
  return (
    
    <div><Routes>
      <Route path='/' element={<Home/>}/>
      <Route path='/inner' element={<Inner/>}/>
    </Routes>
    
  </div>
  );
}

export default App;
